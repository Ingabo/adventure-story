This is a choose your own adventure comand-line story where players/readers choose how they want their story to
progress. They get to select from a list of options that appear after every scene. Stories will differ from reader
to reader.
This project was inspired by popular anthology series, Black Mirror, specifically the episode known as Bandersnatch. 
The scenes are not exhaustive and readers are encouraged to add more scenes and more options to select from to make
the entire story as diverse as possible.